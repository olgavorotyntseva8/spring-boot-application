package com.company.service;

import com.company.entity.Language;
import com.company.mapper.EntityDTOMapper;
import com.company.model.LanguageCreateRecord;
import com.company.model.LanguageUpdateRecord;
import com.company.model.Response;
import com.company.repository.LanguageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LanguageService {

    private final LanguageRepository languageRepository;

    private final EntityDTOMapper entityDTOMapper;

    public List<LanguageCreateRecord> getAllLanguages() {
        return languageRepository.findAll().stream()
                .map(entityDTOMapper::languageToLanguageRecord)
                .collect(Collectors.toList());
    }

    public LanguageCreateRecord getLanguageByName(String languageName) {
        Language language = languageRepository.findByName(languageName);
        return entityDTOMapper.languageToLanguageRecord(language);
    }

    public Response<LanguageCreateRecord> addLanguage(LanguageCreateRecord languageRecord) {
        Response<LanguageCreateRecord> result = new Response<>();
        result.setBody(languageRecord);
        boolean isExist = languageRepository.existsByName(languageRecord.getName());
        try {
            if (isExist) {
                throw new IllegalArgumentException("Language is already exist");
            }
            Language language = entityDTOMapper.languageRecordToLanguage(languageRecord);
            languageRepository.save(language);
            result.setState(HttpStatus.ACCEPTED);
        } catch (IllegalArgumentException e) {
            result.setState(HttpStatus.INTERNAL_SERVER_ERROR);
            result.getErrors().add(e.getMessage());
        }
        return result;
    }

    public Response<LanguageUpdateRecord> updateLanguage(String name, LanguageUpdateRecord languageRecord) {
        Response<LanguageUpdateRecord> result = new Response<>();
        result.setBody(languageRecord);
        try {
            Language language = languageRepository.findByName(name);
            if (language == null) {
                throw new IllegalArgumentException("Language not found");
            }
            language = updateLanguage(language, languageRecord);
            languageRepository.save(language);
            result.setState(HttpStatus.ACCEPTED);
        } catch (IllegalArgumentException e) {
            result.setState(HttpStatus.INTERNAL_SERVER_ERROR);
            result.getErrors().add(e.getMessage());
        }
        return result;
    }

    @Transactional
    public Response<?> deleteLanguage(String name) {
        Response<LanguageUpdateRecord> result = new Response<>();
        try {
            languageRepository.deleteByName(name);
            result.setState(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            result.setState(HttpStatus.INTERNAL_SERVER_ERROR);
            result.getErrors().add(e.getMessage());
        }
        return result;
    }

    private Language updateLanguage(Language oldLanguage, LanguageUpdateRecord languageRecord) {
        return oldLanguage.toBuilder()
                .description(languageRecord.getDescription())
                .rating(languageRecord.getRating())
                .build();
    }
}
