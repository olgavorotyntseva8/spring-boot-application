package com.company.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class LanguageRecord {

    private String description;

    @Min(value = 1, message = "Incorrect rating value. Value must be more or equals 1")
    @Max(value = 5, message = "Incorrect rating value. Value must be less or equals 5")
    private Integer rating;
}
