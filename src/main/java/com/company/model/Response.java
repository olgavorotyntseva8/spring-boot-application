package com.company.model;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> {

    private HttpStatus state;

    private T body;

    private List<String> errors;

}
