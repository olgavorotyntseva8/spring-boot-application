package com.company.model;

import com.company.utils.IsCorrectLanguageName;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"name", "description", "rating" })
public class LanguageCreateRecord extends LanguageRecord {

    @IsCorrectLanguageName(message = "Incorrect name value")
    private String name;

}
