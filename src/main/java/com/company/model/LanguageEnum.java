package com.company.model;

public enum LanguageEnum {
    JAVA("Java"),
    PYTHON("Python"),
    JAVASCRIPT("JavaScript"),
    CSHARP("C#"),
    CPLUSPLUS("C++");

    private String name;

    LanguageEnum(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
