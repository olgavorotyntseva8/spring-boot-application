package com.company.repository;

import com.company.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Integer> {

    Language findByName(String name);

    void deleteByName(String name);

    boolean existsByName(String name);

}
