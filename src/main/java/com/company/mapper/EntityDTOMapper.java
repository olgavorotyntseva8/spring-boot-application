package com.company.mapper;

import com.company.entity.Language;
import com.company.model.LanguageCreateRecord;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
@Component
public interface EntityDTOMapper {

    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "rating", target = "rating")
    })
    LanguageCreateRecord languageToLanguageRecord(Language language);

    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "rating", target = "rating")
    })
    Language languageRecordToLanguage(LanguageCreateRecord languageRecord);
}
