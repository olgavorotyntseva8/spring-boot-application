package com.company.controller;

import com.company.model.LanguageCreateRecord;
import com.company.model.LanguageUpdateRecord;
import com.company.model.Response;
import com.company.service.LanguageService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/language")
@Validated
public class LanguageController {

    private final LanguageService languageService;

    @GetMapping("")
    public List<LanguageCreateRecord> getAllLanguages() {
        return languageService.getAllLanguages();
    }

    @GetMapping("/{languageName}")
    public LanguageCreateRecord getLanguageByName(@PathVariable String languageName) {
        return languageService.getLanguageByName(languageName);
    }

    @PostMapping("")
    public Response<LanguageCreateRecord> addLanguage(@RequestBody @Valid LanguageCreateRecord languageRecord) {
        return languageService.addLanguage(languageRecord);
    }

    @PutMapping("/{languageName}")
    public Response<LanguageUpdateRecord> updateLanguage(@PathVariable String languageName,
                                                         @RequestBody @Valid LanguageUpdateRecord languageRecord) {
        return languageService.updateLanguage(languageName, languageRecord);
    }

    @DeleteMapping("/{languageName}")
    public Response<?> deleteLanguage(@PathVariable String languageName) {
        return languageService.deleteLanguage(languageName);
    }


}
