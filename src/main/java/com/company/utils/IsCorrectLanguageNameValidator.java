package com.company.utils;

import com.company.model.LanguageEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class IsCorrectLanguageNameValidator implements ConstraintValidator<IsCorrectLanguageName, String> {

    private static List<String> possibleValues;

    static {
        String[] values = Arrays.stream(LanguageEnum.class.getEnumConstants()).map(LanguageEnum::getName).toArray(String[]::new);
        possibleValues = Arrays.asList(values);
    }

    @Override
    public void initialize(IsCorrectLanguageName constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null || value.isEmpty()) {
            return false;
        }
        if (possibleValues.contains(value)) {
            return true;
        }
        return false;
    }

}
