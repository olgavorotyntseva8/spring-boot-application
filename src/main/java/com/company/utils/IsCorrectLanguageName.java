package com.company.utils;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {IsCorrectLanguageNameValidator.class})
public @interface IsCorrectLanguageName {

    String message() default "Language name is incorrect";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
